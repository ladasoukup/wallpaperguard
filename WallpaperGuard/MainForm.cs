﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Management;

namespace WallpaperGuard
{
    public partial class MainForm : Form
    {
        public string[] CmdArgs;
        public MainForm(string[] args)
        {
            CmdArgs = args; ;
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            app_notify.Visible = true;
            app_notify.Text = Application.ProductName + " " + Application.ProductVersion.ToString();
            app_timer.Enabled = true;
            timer_start.Enabled = true;
            timer_start.Interval = 100;
            Application.DoEvents();
        }

        private void app_notify_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // TODO: Dotaz na heslo pro ukonceni...
            // Application.Exit();
        }

        private void app_timer_Tick(object sender, EventArgs e)
        {
            CallSetWallpaper();
        }

        private void timer_start_Tick(object sender, EventArgs e)
        {
            timer_start.Enabled = false;
            this.WindowState = FormWindowState.Minimized;
            this.Visible = false;
            CallSetWallpaper();
        }

        private object GetHWInfo(string Query, string Variable)
        {
            object RetValue = "0";
            ManagementObjectSearcher mos = new ManagementObjectSearcher(Query);

            foreach (ManagementObject mob in mos.Get())
            {
                foreach (PropertyData pyd in mob.Properties)
                {
                    if (pyd.Name == Variable) RetValue = pyd.Value;
                }
            }

            return RetValue;
        }

        public string GetNetworkInformation()
        {
            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection moc = mc.GetInstances();
            string NetworkCardInfo = String.Empty;
            string[] IP;
            foreach (ManagementObject mo in moc)
            {
                if ((bool)mo["IPEnabled"] == true)
                {
                    IP = (string[])mo["IpAddress"];
                    NetworkCardInfo += IP[0] + " " + mo["MacAddress"].ToString() + " (" + mo["Caption"].ToString() + ")\n";
                }
                mo.Dispose();
            }
            return NetworkCardInfo;
        }

        private void DoSetWallpaper(Bitmap wallpaper_img)
        {
            string[] TextOutput = new string[50];
            
            System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.White);
            System.Drawing.Font drawFontB = new System.Drawing.Font("Tahoma", 22, FontStyle.Bold);
            System.Drawing.Font drawFont = new System.Drawing.Font("Tahoma", 16, FontStyle.Regular);


            TextOutput[0] = GetHWInfo("SELECT Name FROM Win32_Processor", "Name").ToString().Trim();
            TextOutput[1] = "Memory: \t " + (Convert.ToInt32(GetHWInfo("SELECT TotalPhysicalMemory FROM Win32_ComputerSystem", "TotalPhysicalMemory")) / 1024 / 1024) + " MB";
            TextOutput[2] = "Snapsht time: \t " + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToLongTimeString();
            TextOutput[4] = "Network cards:";

            string[] Arr = GetNetworkInformation().Split('\n');
            int loop = 5;
            foreach (string NetworkCard in Arr)
            {
                TextOutput[loop] = NetworkCard;
                loop++;
            }


            using (Graphics grfx = Graphics.FromImage(wallpaper_img))
            {
                //draw on it as you wish
                grfx.DrawString(System.Environment.UserDomainName + "\\" + System.Environment.MachineName + " - " + System.Environment.UserName, drawFontB, drawBrush, 50, 50);
                grfx.DrawString("----------------------------------------------------------------------", drawFont, drawBrush, 50, 70);

                loop = 0;
                foreach (string Line in TextOutput)
                {
                    grfx.DrawString(Line, drawFont, drawBrush, 50, (90 + loop * 20));
                    loop++;
                }

            }

            wallpaper.Set(wallpaper_img, wallpaper.Style.Stretched);
        }


        private void CallSetWallpaper()
        {
            // Set WallPaper...
            if (CmdArgs.Length > 0)
            {
                if ((CmdArgs[0] == "?") || (CmdArgs[0] == "help"))
                {
                    string HelpString;
                    HelpString = "syntax:  wallpaperguard.exe <wallpaper> <stay>\r\n";
                    HelpString += "\r\n";
                    HelpString += "<wallpaper>: backup, city, blanik, oldies\r\n";
                    HelpString += "\r\n";
                    HelpString += "<stay>: leave empty to just set the wallpaper, or insert text \"stay\" to keep Wallpaper Guard running..\r\n";
                    MessageBox.Show(HelpString, "Wallpaper Guard - HELP", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Application.Exit();
                }
                else if (CmdArgs[0] == "city")
                {
                    DoSetWallpaper(Properties.Resources.city);
                }
                else if (CmdArgs[0] == "blanik")
                {
                    DoSetWallpaper(Properties.Resources.blanik);
                }
                else if (CmdArgs[0] == "oldies")
                {
                    DoSetWallpaper(Properties.Resources.oldies);
                }
                else if (CmdArgs[0] == "olympic")
                {
                    DoSetWallpaper(Properties.Resources.oldies);
                }

                else if (CmdArgs[0] == "backup")
                {
                    DoSetWallpaper(Properties.Resources.blanik);
                }

                else
                {
                    //default
                    DoSetWallpaper(Properties.Resources.city);
                }

                if (CmdArgs.Length > 1)
                {
                    if (CmdArgs[1] != "stay") Application.Exit();
                }
                else
                {
                    Application.Exit();
                }

            }
            else
            {
                //default
                DoSetWallpaper(Properties.Resources.city);
                Application.Exit();
            }
        }
    }
}